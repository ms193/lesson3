package az.ingress.demo.controller;

import org.springframework.web.bind.annotation.*;

@RestController

public class HelloController {
    @GetMapping("/hello")

    public String hello(@RequestParam (required = false) String name,
                         @RequestHeader (value = "Accept-Language")String lang){
        return switch (lang){
            case "az" ->"Salam " + name;
            case "ru" -> "Privet ot " + name;
            case "en" -> "Hello from " + name;
            default -> "Hello";
        };
    }
    @PostMapping("/create")
    public Student create(@RequestBody Student student){
        System.out.println("Student created" + student );
        return student;

    }
    @PutMapping("/update")
    public void update(@RequestBody Student student){
        System.out.println("Student updated " + student);
    }
    @DeleteMapping("/student/{id}/delete")

    public void delete(@PathVariable Integer id){
        System.out.println("Student deleted");
    }

}
